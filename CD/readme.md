# Zabezpečení webové aplikace

Tyto soubory jsou součástí bakalářské práce Davida Pokorného

## Obsah CD

Adresářová struktura přiložených souborů:

```bash
/
├── documentation          # zdrojové kódy bakalářské práce
│   └── sources            # ukázky kódů použité v práci
├── readme.md              # tento soubor
├── sources                # implementační soubory
│   ├── formplugin         # formulář pro práci s pluginy
│   ├── forms              # ukázka formulářů použitých pro MAF
│   └── rbac_setup.php     # nastavení rolí a oprávnění pro MAF
└── thesis.pdf             # hlavní dokument bakalářské práce
```
Adresářová struktura složky /sources/formplugin
```bash
├── Classes					# složka pro třídy pluginu
│   └── Form.php			# třída Form
├── Examples				# složka pro ukázky použití pluginu
│   └── form-example.php	# ukázka použití pluginu
├── FormPlugin.php			# inicializační soubor pluginu
├── Namespaces				# složka s definicemi funkcí pro plugin
│   ├── errors.php			# chybové funkce
│   ├── filters.php			# filtry pro uživatelský vstup	
│   └── terms.php			# funkce pro kontrolu uživatelského vstupu
└── readme.txt				# stručný popis FormPluginu pro repozitář WP
```

## Odkaz na práci
Práci lze získat pomocí Git:

```bash
git clone https://Bagnat@bitbucket.org/Bagnat/bakalarska-prace.git
```

Popřípadě samotný plugin na práci s formuláři je k dispozici pomocí: 
```bash
git clone https://Bagnat@bitbucket.org/bagnatcz/formplugin.git
```

## Odevzdaná verze
Odevzdané verze bakalářské práce jsou v Git repozitářích označeny větví **bc-odevzdano**. Pro stažení odevzdané verze lze použití následující příkazy:
```bash
git clone -b bc-odevzdano https://Bagnat@bitbucket.org/Bagnat/bakalarska-prace.git
git clone -b bc-odevzdano https://Bagnat@bitbucket.org/bagnatcz/formplugin.git
```