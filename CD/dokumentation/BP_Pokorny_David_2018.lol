\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.1}MAF certifikát}{10}{lstlisting.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.2}Přesměrování pomocí htaccess}{12}{lstlisting.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.1}Ukázka stránky zranitelné na lokální XSS}{17}{lstlisting.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.2}Ukázka stránky zranitelné na stálý XSS}{19}{lstlisting.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.3}Náhrada speciálních znaků na HTML entity}{20}{lstlisting.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.4}Příklad HTTP hlavičky}{22}{lstlisting.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3.5}Použití \$wpdb}{28}{lstlisting.3.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.1}Přiřazení názvu oprávnění k jednotlivým akcím článku}{30}{lstlisting.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.2}Odebrání role}{31}{lstlisting.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.3}Přidání role}{31}{lstlisting.4.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.4}Přiřazení oprávnění}{31}{lstlisting.4.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.5}Odebrání generator tagu}{33}{lstlisting.4.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.1}FormPlugin -- Definování kontrol}{42}{lstlisting.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.2}FormPlugin -- Přiřazení kontrol k formuláři}{43}{lstlisting.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.3}FormPlugin -- Kontrola formuláře}{44}{lstlisting.5.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.4}FormPlugin -- Save funkce -- kontroly}{45}{lstlisting.5.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.5}FormPlugin -- Definování filtrů}{45}{lstlisting.5.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.6}FormPlugin -- Přiřazení filtrů k formuláři}{46}{lstlisting.5.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.7}FormPlugin -- Save funkce -- filtry}{46}{lstlisting.5.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.8}Odebrání a přidání rolí}{47}{lstlisting.5.8}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.9}Přidání oprávnění adminovi a manažerovi}{48}{lstlisting.5.9}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.10}Přidání oprávnění k taxonomy}{49}{lstlisting.5.10}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.11}Registrace taxonomie}{49}{lstlisting.5.11}% 
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.12}Přidání dalších oprávnění}{50}{lstlisting.5.12}% 
