\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {czech}
\defcounter {refsection}{0}\relax 
\select@language {czech}
\defcounter {refsection}{0}\relax 
\select@language {czech}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{vi}{section*.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Seznam výpisů kódu}{xi}{section*.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{{\' U}vod}{1}{chapter*.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{1}{section*.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{1}{section*.8}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{3}{section*.10}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Seznámení se s projektem}{5}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{5}{section*.11}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{5}{section*.12}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Základní zabezpečení webových projektů}{7}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Verzování}{7}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{7}{section*.13}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}{section*.14}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Vytvoření testovací verze}{8}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}{section*.15}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{8}{section*.16}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Zabezpečení domény a spojení}{9}{section.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}HTTPS}{9}{subsection.2.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{9}{section*.17}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Implementace HTTPS}{10}{subsection.2.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{11}{section*.18}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{11}{section*.19}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{11}{section*.20}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{11}{section*.21}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Vynucení HTTPS}{11}{subsection.2.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{11}{section*.22}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{12}{section*.23}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{12}{section*.24}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{13}{section*.25}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}Zabezpečení vstupů}{15}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}{section*.26}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Validace vstupních dat}{15}{section.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Riziko nevalidace dat}{15}{subsection.3.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{15}{section*.27}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Validace dat}{16}{subsection.3.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}{section*.28}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{16}{section*.29}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}{section*.30}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Cross-site scripting (XSS)}{17}{section.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Lokální / XSS Type 0}{17}{subsection.3.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{17}{section*.31}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1.1}Důsledky lokálního XSS}{18}{subsubsection.3.2.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}{section*.32}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1.2}Protiopatření proti lokálnímu XSS}{18}{subsubsection.3.2.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{18}{section*.33}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Stálý / XSS Type 1}{19}{subsection.3.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2.1}Ochrana proti stálému XSS}{20}{subsubsection.3.2.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{20}{section*.34}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Zrcadlený / XSS Type 2}{21}{subsection.3.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3.1}Ochrana proti zrcadlenému XSS}{21}{subsubsection.3.2.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}HTTP response splitting}{21}{section.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{22}{section*.35}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Session hijacking}{22}{section.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{22}{section*.36}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{23}{section*.37}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{23}{section*.38}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Fyzické zkopírování SID}{23}{subsection.3.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{23}{section*.39}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Session sidejacking/sniffing}{23}{subsection.3.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{24}{section*.40}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Session fixation attack}{24}{subsection.3.4.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{24}{section*.41}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{24}{section*.42}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{24}{section*.43}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.4}Cross-site scripting}{25}{subsection.3.4.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{25}{section*.44}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{25}{section*.45}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}SQL injection}{26}{section.3.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{27}{section*.46}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Ochrana před SQL injection}{27}{subsection.3.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{28}{section*.47}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {4}Bezpečnost systému WordPress}{29}{chapter.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Role-based access control}{29}{section.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{29}{section*.48}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Instalace WP}{31}{section.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Změna výchozích hodnot}{32}{subsection.4.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{32}{section*.49}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{32}{section*.50}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{32}{section*.51}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Skrytí dodatečných informací}{32}{subsection.4.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{33}{section*.52}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Deaktivace nebezpečných funkcí}{33}{subsection.4.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{33}{section*.53}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{34}{section*.54}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Přístupová práva k souborům}{34}{subsection.4.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Zabezpečení WordPress}{35}{section.4.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Wordfence Security -- Firewall \& Malware Scan}{35}{subsection.4.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.1}Blokování malicious traffic}{35}{subsubsection.4.3.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{36}{section*.55}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.2}Malware skener pro požadavky na server}{36}{subsubsection.4.3.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.3}Ochrana proti brute force útoku}{36}{subsubsection.4.3.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{36}{section*.56}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.4}Kontrola zdrojových kódu WP, šablony a pluginů}{36}{subsubsection.4.3.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.5}Kontrola ostatních souborů kvůli virům, podezřelým kódům a odkazování/přesměrování}{37}{subsubsection.4.3.1.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.6}Skenování příspěvků a komentářů}{37}{subsubsection.4.3.1.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.7}Kontrola nastavení WP, šablon a pluginů}{37}{subsubsection.4.3.1.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.8}Ověření aktuálních verzí}{37}{subsubsection.4.3.1.8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1.9}Další kontroly}{38}{subsubsection.4.3.1.9}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{38}{section*.57}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Salt Shaker}{38}{subsection.4.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{38}{section*.58}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Záloha}{38}{subsection.4.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{38}{section*.59}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{39}{section*.60}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3.1}UpdraftPlus WordPress Backup Plugin}{39}{subsubsection.4.3.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{39}{section*.61}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3.2}Jetpack od WordPress.com}{39}{subsubsection.4.3.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3.3}BackupBuddy}{40}{subsubsection.4.3.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {5}Implementace zabezpečení}{41}{chapter.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{41}{section*.62}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Validace vstupů}{42}{section.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Bezpečné ukládání vstupů}{45}{section.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}SQL injection}{47}{section.5.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Nastavení rolí a oprávnění}{47}{section.5.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{47}{section*.63}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{48}{section*.64}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{49}{section*.65}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{50}{section*.66}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Z{\' a}v{\v e}r}{51}{chapter*.67}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{51}{section*.68}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{51}{section*.69}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {6}Seznam použitých zkratek}{53}{chapter.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {7}Obsah přiloženého CD}{55}{chapter.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{55}{section*.70}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografie}{57}{section*.72}% 
