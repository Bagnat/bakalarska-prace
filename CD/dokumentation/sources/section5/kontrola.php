<?php // /sources/formplugin/Classes/Form.php, řádky 98-120

$errors = array();
if( $this->podminky ) //pokud jsou nějaké podmínky přiřazené
{
	foreach($this->podminky as $name => $detail){
	    // \$name\verb|    |= název vstupu
	    // \$detail\verb|  |= pole podmínek
	    
		//není povinný a nemá další omezení
		if( [/*je nepovinný AND není nastavený*/]) continue;
		
		foreach ($detail as $function => $param){
			// \$function\verb|   |= term		(např.: 'type')
			// \$param\verb|      |= hodnota	(např.: 'string')
			
			$funkce = "FormPluginTerms\\".$function;
			if(function_exists( $funkce )){
				if( ! $funkce( $input[$name], $param) )
					$errors[$name][] = $function;
			}else{
				if(!isset($errors["terms"])) 
					$errors["terms"] = array();
				$errors["terms"][] = $function;
			}
		}
	}
}