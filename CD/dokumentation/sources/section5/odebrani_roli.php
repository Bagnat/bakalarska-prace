<?php // /sources/rbac\_setup.php, řádky 10-24

// odebrání rolí
$odebrat = array('editor', 'author', 'contributor', 
			'subscriber', 'manager', 'redaktor', 'hrac');
foreach ($odebrat as $role){
	if( get_role($role) ){
		remove_role( $role );
	}
}

// přidání rolí
$pridat = array('manager' => "`Manažer`", 'redaktor' => "`Redaktor`", 
			'hrac' => "`Hráč`");
foreach ($pridat as $slug => $name){
	add_role( $slug, __( $name ) );
}