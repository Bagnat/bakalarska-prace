<?php // /sources/forms/form-registrace.php
function form_registrace_terms()
{
	return array(
		"nick" => array(			//název vstupu
			"requied"	=> true,	//zda je vyžadován
			"min_len"	=> 3,		//minimální délka
			"type"		=> "string"	//datový typ
		),
		"mail" => array(
			"min_len"	=> 6,
			"type"		=> "email"
		),
		"passwd" => array(
			"type"		=> "array",
			"count"		=> 2,		// pole o 2 prvcích
			"equal"		=> true		// itemy pole se musí rovnat
		),
		[...]
	);
}
