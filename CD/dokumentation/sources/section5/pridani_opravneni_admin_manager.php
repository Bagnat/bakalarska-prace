<?php // /sources/rbac\_setup.php, řádky 30-44

global $wp_roles; // global class wp-includes/capabilities.php
$opravneni = array(
	'edit_{slug}', 'read_{slug}', 'delete_{slug}', 
	'edit_{slug}s', 'edit_others_{slug}s', 
	'publish_{slug}s', 'read_private_{slug}s', 'edit_{slug}s'
);

$roles = array('administrator', 'manager');

$typyClanku = array("cvik", "ukol", "doplnek", 
				"turnaj", "legie", "clanek", "post");

foreach ($roles as $role) {
	foreach ($typyClanku as $typ){
		foreach ($opravneni as $name){
			$cap = str_replace("{slug}", $typ, $name);
			$wp_roles->add_cap( $role, $cap, true );
		}
	}
}