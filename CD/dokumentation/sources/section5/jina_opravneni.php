<?php // /sources/rbac\_setup.php, řádky 80-94

$pravidla = array(
	'edit_users'            => array('administrator', 'manager'),
	'read'                  => array('administrator', 'manager',
															'redaktor'),	
	'admin_chyby'           => array('administrator'),
	'admin_kvizy'           => array('administrator', 'manager'),
	'admin_trener'          => array('administrator', 'manager'),
	'admin_propojeni_ukolu' => array('administrator', 'manager'),
	'admin_mail'            => array('administrator', 'manager')
);

foreach ($pravidla as $cap => $roles){
	foreach ($roles as $role){
		// \$role\verb|   |-> název role
		// \$cap\verb|    |-> název oprávnění
		$wp_roles->add_cap( $role, $cap, true );
	}
}