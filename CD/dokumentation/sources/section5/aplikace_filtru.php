<?php // /sources/formplugin/Classes/Form.php, řádky 122-149

if( $this->filtry ) {  //pokud jsou nějaké filtry přiřazené
	
	foreach($this->filtry as $name => $filters) {
	
		//filtr se provede, pouze pokud vstup obstál u všech podmínek
		if( !isset($errors[ $name ]) ){
		
			//není nastavený nebo povinný
			if( !isset($input[$name]) || $input[$name] === null)
				continue;
			
			foreach ($filters as $filtr)
			{
				$funkce = "FormPluginFilters\\".$filtr;
				if( function_exists( $funkce ) )
					$input[$name] = $funkce( $input[$name] );
			}
		
		}else
			$errors[$name]["filters"] = true;
	}
}