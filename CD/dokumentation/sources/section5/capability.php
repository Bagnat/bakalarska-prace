<?php
$labels = array(
	'name'	=> __( 'Kategorie' ),  [...]	
);

$args = array(
	'labels'				=> $labels, [...],	
	'capabilities'	=> array(
		'manage_terms'	=> 'manage_categories_doplnek_kategorie',
		'edit_terms'		=> 'manage_categories_doplnek_kategorie',
		'delete_terms'	=> 'manage_categories_doplnek_kategorie',
		'assign_terms'	=> 'manage_categories_doplnek_kategorie',
	)
);

register_taxonomy( 'kategorie_doplnku', 'doplnek' , $args );