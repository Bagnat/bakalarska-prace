<?php // /sources/rbac\_setup.php, řádky 68-77

$roles = array('administrator', 'manager');
$typyTaxonomy = array("clanek_tag", "clanek_kategorie", 
			"cvik_kategorie", "doplnek_kategorie");
$opravneni = "manage_categories_{slug}";

foreach ($roles as $role) {
	foreach ($typyTaxonomy as $typ){
		$cap = str_replace("{slug}", $typ, $opravneni);
		$wp_roles->add_cap( $role, $cap, true );
	}
}