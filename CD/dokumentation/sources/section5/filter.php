<?php // /sources/formplugin/Namespaces/filters.php
namespace FormPluginFilters;

//Upraví text na povolený obsah
function user_text_content($value){
    return htmlspecialchars($value);
}

//Upraví pole na povolený obsah
function user_text_content_array($value){
    foreach ($value as &$v) $v = htmlspecialchars($v);
    return $value;
}