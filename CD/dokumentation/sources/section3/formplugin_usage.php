<h1>Jednoduchá registrace:</h1>
<?php

    $form = new form("registrace");
    $result = $form->save_form();

    if($result === null){
        //formulář nebyl ukládán
        echo $form->get_form();

    }else if($result === true){
        //formulář byl odeslán a skončil úspěšně
        echo "Úspěšná registrace";

    }else if($result === false){
        //formulář byl odeslán, ale skončil špatně (neprošly podmínky)
        echo "Zkontrolujte prosím vyplněná data";
        echo $form->get_form();
    }

?>