<?php

function form_registration_echo($hidden)
{
    return "
        <form method='post'>
            Vaše jméno:     <input type='text'      name='registration-name'><br>
            Váš věk:        <input type='number'    name='registration-age'><br>
            Vaše heslo:     <input type='password'  name='registration-passwd[]'><br>
            Kontrola hesla: <input type='password'  name='registration-passwd[]'>
            {$hidden}
        </form>
    ";
}

function form_registration_save($data, $errors)
{
    if($errors) {
        //Zpracování chyb
    }
    else{
        //Zpracování dat
        $name = $data["name"];
        return true;
    }

    return false;
}

function form_registration_terms()
{
    return array(
        "name" => array(
            "requied" => true,
            "min_len" => 3,
            "type"    => "string"
        ),
        "numeric" => array(
            "requied" => true,
            "type"    => "numeric",
            "min_val" => 1,
            "max_val" => 110
        ),
        "passwd" => array(
            "requied" => true,
            "type"    => "array",
            "subtype" => "string",
            "equal"   => true
        )
    );
}

?>