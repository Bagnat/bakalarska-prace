<?php
if(isset($_POST["visitor"])){
	//bezpečné uložení jména a poznámky do dtb
	//můžeme data zkontrolovat na datový typ
	[...]
}

$visits = [...] //načtení vzkazů z dtb
foreach ($visits as $visit)
	echo "{$visit["name"]}: {$visit["comment"]}<br>";
/*
 * `Možný výstup na stránce`:
 * `David: Děkuji za přivítání`
 * `Přemysl: Moc pěkné stránky.`
 */
?>