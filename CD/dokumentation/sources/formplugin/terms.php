<?php // /sources/formplugin/Namespaces/terms.php
namespace FormPluginTerms;

//Kontrola datového typu / významu
function type($value, $param){
    switch($param){
        case("string"): return is_string($value);
        [...]
        case("email"): return 
			filter_var($value, FILTER_VALIDATE_EMAIL) 
			? true : false;
    }
    return null;
}

//Zda je pole povinné
function requied($value, $param = true){
    if($param === false) return true;
    return $value !== null;
}

//Zda je to číslo a zda je vetší rovno parametru
function min_val($value, $param){
    return type($value, "numeric") && $value >= $param;
}