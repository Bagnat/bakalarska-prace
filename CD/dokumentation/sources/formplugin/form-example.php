<?php

/*
funkce, které podporuji
function form-name-save() {}
function form-name-echo($hidden) {}

*/

/*
 * zde lze využít 2. parametru u get_form, který se přidá do volání form_example_echo ajko druhý parametr ( $trow )
 */
function form_example_echo($hidden)
{
    ?>
        <form> Formulář může být jak POST tak GET
        <input type='text' name='jmeno'> toto nedostaneš v data v funkci save
            <input type='text' name='example-jmeno'> toto budeš mít v data['jmeno']
            <input type='text' name='example-mail'> toto budeš mít v data['mail']
        <input type='submit' name=''>
        {$hidden}
        </form>

    <?php
}


//pokud je formulář bez podmínek
//function form_example_save($data)
//{
//    echo "tohle se vypíše při uložení formuláře<br>{$data["jmeno"]}";
//}


//pokud je formulář s podmínkami
function form_example_save($data, $errors)
{
    echo "tohle se provede při uložení formuláře (v místě, kde vytváříš objekt formuláře<br>{$data["jmeno"]}<br>
            v errors jsou uloženy chyby, kde byly stanoveny a nesplněny podmínky";
    print_r($errors);
}




function form_example_terms() //tímto lze nahradit defaultní podmínky v případě, že se žádne nepředají explicitně
{
    return array(
        "jmeno" => array(       //název inputu (v html to je name='example-jmeno')
            "requied" => true,      //je to vyžadováno (není potřeba uvádět, ty názvy, co v podmínkách nemají zastoupení jsou volitelné - např. name='example-mail' je volitelný)
            "min_len" => 3,          //minimální délka
            "min_val" => 3,          //minimální hodnota
            "min_array" => 2,          //minimální count pole - pouze pro array
            "max_array" => 2,          //maximální count pole - pouze pro array (jinak nemá vliv)
            "equal" => true,          //zda se hodnoty v poli sobě musí rovnat (kontrola hesla) - pouze pro array
            "type"    => "string"   //datový typ - string | numeric | array
            //v případě array se všechny min kontrolují pro všechny pole
        )
    );
}





//---------Toto vložíte do stránky, kde chcete formulář-------------//

//vytvoření instance bez podmínek
$form = new Form("example");


//vytvoření instance s podmínkami
$podminky = array(
    "jmeno" => array(       //název inputu (v html to je name='example-jmeno')
        "requied" => true,      //je to vyžadováno (není potřeba uvádět, ty názvy, co v podmínkách nemají zastoupení jsou volitelné - např. name='example-mail' je volitelný)
        "min_len" => 3,          //minimální délka
        "min_val" => 3,          //minimální hodnota
        "min_array" => 2,          //minimální count pole - pouze pro array
        "equal" => true,          //zda se hodnoty v poli sobě musí rovnat (kontrola hesla) - pouze pro array
        "type"    => "string"   //datový typ - string | numeric | array | file
        //v případě array se všechny min kontrolují pro všechny pole
    )
);
$form = new Form("example", $podminky); // $podmínky lze nahradit funkcí form_example_terms()

//vypsání formuláře
$form->get_form();

//uložení formuláře
$form->save_form();

?>