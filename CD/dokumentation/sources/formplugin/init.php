<?php

function form_neregistrovane_echo($hidden)
{
        trigger_error ( "FormPlugin říká: voláte echo funkci, která nebyla deklarována." , E_USER_WARNING );
}

function form_neregistrovane_save()
{
    trigger_error ( "FormPlugin říká: voláte save funkci, která nebyla deklarována." , E_USER_WARNING );
}

function form_neregistrovane_result()
{
    trigger_error ( "FormPlugin říká: voláte result funkci, která nebyla deklarována." , E_USER_WARNING );
}

class Form
{
    public $file = "";          ///<Název formuláře
    public $echo = "";          ///<název echo funkce
    public $result = "";
    public $save = "";          ///<název save funkce
    public $podminky = null;
    public $filtry = null;
    public $hidden;             ///<input pro spuštení save

    /**
     * Form constructor.
     * @param $file string název formuláře
     * @param $podminky array podmínky (lze je nedat explicitně)
     */
    function __construct($file, $podminky = null, $filtry = null)
    {

        if(file_exists ( FP_FORM_FOLDER."/form-".$file.".php" ))
        {
            $this->file = $file;

            $this->hidden = "<input type='hidden' name='form-{$this->file}-hidden' value='true'>";

            if(include_once(FP_FORM_FOLDER."/form-".$file.".php"))


            //kontrola deklarovaných funkcí v souboru form-name-xxx
            if( function_exists ( "form_".$this->file."_echo" ) ) $this->echo = "form_".$this->file."_echo";
                            else $this->echo = "form_neregistrovane_echo";

            if( function_exists ( "form_".$this->file."_save" ) ) $this->save = "form_".$this->file."_save";
                            else $this->save = "form_neregistrovane_save";

            if( function_exists ( "form_".$this->file."_result" ) ) $this->result = "form_".$this->file."_result";
                            else $this->result = "form_neregistrovane_result";

            if(!$podminky AND function_exists ( "form_".$this->file."_terms" ) ) $this->podminky = call_user_func("form_".$this->file."_terms");
            else $this->podminky = $podminky;

            if(!$filtry AND function_exists ( "form_".$this->file."_filters" ) ) $this->filtry = call_user_func("form_".$this->file."_filters");
            else $this->filtry = $filtry;

            
        }
        else{
            trigger_error ( "FormPlugin říká: soubor form-{$file}.php se nenachází v ".FP_FORM_FOLDER , E_USER_WARNING );
        }
    }

    /**
     * @param $throw = data, která budou k dostání ve funkci v druhém parametru
     */
    public function get_form($throw = NULL)
    {
        $name = $this->echo;

        if($throw) return $name($this->hidden, $throw);
        else return $name($this->hidden);
    }

    /**
     * @param $throw = data, která budou k dostání ve funkci v druhém parametru
     */
    public function get_result($throw = NULL)
    {
        $name = $this->result;

        if($throw) return $name($throw);
        else return $name();
    }

    public function save_form($data = null)
    {

        if (isset($_POST["form-{$this->file}-hidden"]) || isset($_GET["form-{$this->file}-hidden"]) || isset($data["form-{$this->file}-hidden"])) {

            if(!$data)
            {
                if (isset($_POST["form-{$this->file}-hidden"])) $data = $_POST;
                elseif (isset($_GET["form-{$this->file}-hidden"])) $data = $_GET;
            }

            //pridani attachmentu
            if($_FILES) $data = array_merge($_FILES, $data);

            $input = array();
            foreach ($data as $key => $value) {

                if ($this->file == substr($key, 0, strlen($this->file))) $input[ substr($key, strlen($this->file) + 1)] = $value;
            }

            //kontrola vstupu (podle $podminky)
            $errors = array();
            if( $this->podminky )
            {
                foreach($this->podminky as $name => $detail){

                    //není povinný a není nastavený
                    if( isset($detail["requied"]) && $detail["requied"]===false && (!isset($input[$name]) || $input[$name] === null) ) continue;

                    foreach ($detail as $function => $param){
                        $funkce = "FormPluginTerms\\".$function;
                        if(function_exists( $funkce )){
                            if( ! $funkce( $input[$name], $param) ){
                                $errors[$name][] = $function;
                            }
                        }else{
                            if(!isset($errors["terms"])) $errors["terms"] = array();
                            $errors["terms"][] = $function;
                        }
                    }

                }
            }

            if( $this->filtry )
            {
                foreach($this->filtry as $name => $filters){
                    //filtr se provede, pouze pokud vstup obstál u všech podmínek
                    if(!isset($errors[ $name ])){

                        //není povinný a není nastavený
                        if( isset($detail["requied"]) && $detail["requied"]===false && (!isset($input[$name]) || $input[$name] === null) ) continue;

                        foreach ($filters as $filtr)
                        {
                            $funkce = "FormPluginFilters\\".$filtr;
                            if(function_exists($funkce))
                                $input[ $name ] = $funkce( $input[ $name ] );
                        }

                    }else{
                        $errors[$name]["filters"] = true;
                    }
                }
            }

            if($errors == array()) $errors = null;

            $fce =  $this->save;
            if($this->podminky) return $fce($input, $errors);
            else return $fce($input);
        }
        else return null; //v případě že se neposlal form, tak vrátí null (lze rozeznat null->nic, true->dobře, false->špatne(ale posláno)

    }

    /*
     * vrátí seznam všech form-xxx.php souborů
     */
    static public function get_list()
    {
         $list = scandir(FP_FORM_FOLDER);
         $ret = array();

         foreach($list as $file)
            if(substr($file, 0, 5) == "form-" && substr($file, -4, 4) == ".php")
            {
                 $ret[] = $file;
            }

         if($ret == array()) return NULL;
         else return $ret;
    }

    /**
     * vrátí string s http query, přidá hidden a správně názvy
     * @param $data array dat name => value
     */
    public function get_http_query($data){
        $query = array();
        foreach ($data as $name => $value){
            $query[$this->file."-".$name] = $value;
        }
        $query["form-".$this->file."-hidden"] = "true";
        return http_build_query($query);
    }
}

/**
 * Rychle zkontroluje zda práve tato chyba je přítomna
 * @param $errors array Pole chyb
 * @param $name string Jméno inputu pro kontrolu
 * @param $function string Podminka, kterou kontroluji
 * @return bool Zda je chyba přítomna
 */
function is_this_error($errors, $name, $function){
    return isset($errors[$name]) && in_array($function, $errors[$name]);
}


?>