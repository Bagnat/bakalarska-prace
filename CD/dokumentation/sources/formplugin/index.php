<?php
/**
* Plugin Name: FormPlugin
* Plugin URI:
* Description: řešení pro formuláře
* Version: 0.0
* Author: David Pokorný
* Author URI: davidpoky@gmail.com
* License: GPL
*/

//soubory s formulářem (na konci nepsat "/")
define("FP_FORM_FOLDER", get_template_directory()."/form");

//inicializace
function FormInit() {
    include_once("filters.php");
    include_once("terms.php");
    include_once("init.php");
}
add_action( 'init', 'FormInit');

?>