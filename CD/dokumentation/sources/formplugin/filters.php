<?php
namespace FormPluginFilters;

/**
 * Upraví text na povolený obsah - znefunkční html
 * @param $value
 * @return mixed
 */
function user_text_content($value){
    return htmlspecialchars($value);
}

/**
 * Upraví text na povolený obsah - znefunkční html v poli
 * @param $value
 * @return mixed
 */
function user_text_content_array($value){
    foreach ($value as &$v) $v = htmlspecialchars($v);
    return $value;
}

?>