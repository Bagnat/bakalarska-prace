<?php
$result = add_role(
	'role_name',
	__( 'Role name' ),
	array(						// výčet oprávnění
		'read'         => true,	// povolit oprávnění
		'edit_posts'   => true,
		'delete_posts' => false	// explicitně zakázat
	)
);
if ( null !== $result )
	echo 'Role created';
else
	echo 'Role already exists';
?>