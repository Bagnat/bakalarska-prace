<?php
namespace FormPluginErrors;

function form_neregistrovane_echo($hidden)
{
    trigger_error ( "FormPlugin říká: voláte echo funkci, která nebyla deklarována." , E_USER_WARNING );
}

function form_neregistrovane_save()
{
    trigger_error ( "FormPlugin říká: voláte save funkci, která nebyla deklarována." , E_USER_WARNING );
}

function form_neregistrovane_result()
{
    trigger_error ( "FormPlugin říká: voláte result funkci, která nebyla deklarována." , E_USER_WARNING );
}


/**
 * Rychle zkontroluje zda právě tato chyba je přítomna
 * @param $errors array Pole chyb
 * @param $name string Jméno vstupu pro kontrolu
 * @param $function string Podmínka, kterou kontroluji
 * @return bool Zda je chyba přítomna
 */
function is_this_error($errors, $name, $function){
    return isset($errors[$name]) && in_array($function, $errors[$name]);
}

?>