<?php
/**
* Plugin Name: FormPlugin
* Plugin URI:
* Description: řešení pro formuláře
* Version: 0.0
* Author: David Pokorný
* Author URI: davidpoky@gmail.com
* Tags: form,security,validation
* License: CC BY-NC-SA
* License URI: https://creativecommons.org/licenses/by-nc-sa/3.0/cz/deed.en
**/

//soubory s formulářem (na konci nepsat "/")
define("FP_FORM_FOLDER", get_template_directory()."/form");

//inicializace
function FormInit() {
    include_once("Namespaces/filters.php");
    include_once("Namespaces/terms.php");
    include_once("Namespaces/errors.php");
    include_once("Classes/Form.php");
}
add_action( 'init', 'FormInit');

?>