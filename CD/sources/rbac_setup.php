<?php

/**
 * Nastavení rolí a přiřazení oprávnění
 */ 
function role_and_capabilities(){
	global $wp_roles; // global class wp-includes/capabilities.php

    //OBNOVA ROLI
    if( false )
    {
        $odebrat = array('manager', 'redaktor', 'hrac');
        foreach ($odebrat as $role){
            if( get_role($role) ){
                remove_role( $role );
            }
        }

        //PRIDANI ROLI
        $pridat = array('manager' => "Manažer", 'redaktor' => "Redaktor", 'hrac' => "Hráč");
        foreach ($pridat as $slug => $name){
            add_role( $slug, __( $name ) );
        }
    }


    //PRIDANI CAPABILITIES
    
    
    //ADMIN + MANAGER
    $opravneni = array(
        'edit_{slug}', 'read_{slug}', 'delete_{slug}', 'edit_{slug}s',
        'edit_others_{slug}s', 'publish_{slug}s', 'read_private_{slug}s', 'edit_{slug}s'
    );
    $roles = array('administrator', 'manager');
    $typyClanku = array("cvik", "ukol", "doplnek", "turnaj", "legie", "clanek", "post");
    foreach ($roles as $role) {
        foreach ($typyClanku as $typ){
            foreach ($opravneni as $name){
                $cap = str_replace("{slug}", $typ, $name);
                $wp_roles->add_cap( $role, $cap, true );
            }
        }
    }


    //REDAKTOR na clanek
    $opravneni = array('edit_clanek', 'read_clanek', 'delete_clanek', 'edit_claneks');
    foreach ($opravneni as $cap){
        $wp_roles->add_cap( "redaktor", $cap, true );
    }
    $wp_roles->add_cap( "edit_others_clanek", $cap, false );


    //STRANKY + KOMENTARE
    $stranky = array("edit_pages", "publish_pages", "edit_others_pages",
        "edit_published_pages", "delete_pages", "delete_others_pages", "delete_published_pages",
        "delete_private_pages", "edit_private_pages", "read_private_pages",
        "edit_comment", "moderate_comments");
    $roles = array('administrator', 'manager');
    foreach ($roles as $role){
        foreach ($stranky as $stranka){
            $wp_roles->add_cap( $role, $stranka, true );
        }
    }


    //TAXONOMY
    $roles = array('administrator', 'manager');
    $typyTaxonomy = array("clanek_tag", "clanek_kategorie", "cvik_kategorie", "doplnek_kategorie");
    $opravneni = "manage_categories_{slug}";
    foreach ($roles as $role) {
        foreach ($typyTaxonomy as $typ){
            $cap = str_replace("{slug}", $typ, $opravneni);
            $wp_roles->add_cap( $role, $cap, true );
        }
    }


    //SPECIALNI OPRAVNENI NA STRANKY V ADMINISTRACI + dalsi
    $pravidla = array(
        'edit_users'            => array('administrator', 'manager'),
        'read'                  => array('administrator', 'manager', 'redaktor'),
        'admin_chyby'           => array('administrator'),
        'admin_kvizy'           => array('administrator', 'manager'),
        'admin_trener'          => array('administrator', 'manager'),
        'admin_propojeni_ukolu' => array('administrator', 'manager'),
        'admin_mail'            => array('administrator', 'manager')
    );
    foreach ($pravidla as $cap => $roles){
        foreach ($roles as $role){
            $wp_roles->add_cap( $role, $cap, true );
        }
    }

}

//Zapnu manuálně, pokud je potřeba přegenerovat oprávnění
//add_action('init', 'role_and_capabilities', 10, 0);


?>