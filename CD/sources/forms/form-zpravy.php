<?php

//ODESLANI ZPRAVY
function form_zpravy_echo($hidden, $data)
{
    ?>
    <form class="headerMessage" method="post">
        <div class="newMessageShow">
            <textarea type="text" class="pole" name="zpravy-zprava"></textarea>
            <?= $hidden ?>
            <input type="hidden" name="zpravy-adresat" value="<?= $data["adresat"] ?>">
            <input type="hidden" name="zpravy-prevent" value="<?= time() ?>">
            <button>Odeslat</button>
        </div>
    </form>
    <?php
}

function form_zpravy_save($data, $errors)
{
    if($errors){
        return false;
    }

    //ochrana před odeslání když se zmáčkne f5
    if(isset($_SESSION["form_zpravy_save_prevent"]) && $_SESSION["form_zpravy_save_prevent"] == md5(json_encode($data))) return false;
    $_SESSION["form_zpravy_save_prevent"] = md5(json_encode($data));

    $chat = new chat();
    return $chat->addMessage($data["adresat"], $data["zprava"]);

}

function form_zpravy_terms() //tímto lze nahradit defaultní podmínky v případě, že se žádne nepředají explicitně
{
    return array(
        "adresat" => array(
            "requied"   => true,
            "type"      => "numeric"
        ),
        "zprava" => array(
            "requied"   => true,
            "type"      => "string"
        )
    );
}

function form_zpravy_filters()
{
    return array(
        "zprava" => array("user_text_content")
    );
}
?>