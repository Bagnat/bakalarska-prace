<?php

function form_sparing_echo($hidden)
{
    global $hrac;
    $sparing = get_user_meta($hrac->ID, "sparing", true);
    $place = get_user_meta($hrac->ID, "sparing-place", true);
    $message = get_user_meta($hrac->ID, "sparing-message", true);
    ?>
    <form method="post">
        <div class="form-check">
            <div class="form-group">
                <label for="city">V místě*</label>
                <input type="text" class="form-control" id="ity" placeholder="Brno, Praha" name="sparing-place" value="<?= $place ?>" required>
            </div>

            <div class="form-group">
                <label for="message">Vzkaz</label>
                <textarea class="form-control" id="message" rows="3" name="sparing-message" maxlength="236"><?= $message ?></textarea>
            </div>

            <input class="btn btn-default kvizButton" type="submit" value="<?= !$sparing ? "Přidat se" : "Změnit údaje" ?>">
            <?= $sparing ? '<input class="btn btn-default kvizButton" type="submit" name="sparing-odebrat" value="Odebrat">' : "" ?>
        </div>
        <?= $hidden ?>
    </form>
    <?php
}


function form_sparing_save($data, $errors)
{
    global $hrac;
    if($errors)
    {
        echo "Nevyplnil jste vše. Zpráva musí mít 5 znaků minimálně.";
    }else{
        update_user_meta($hrac->ID, "sparing-place", $data["place"]);
        update_user_meta($hrac->ID, "sparing-message", $data["message"]);

        if($data["place"] && $data["message"]) update_user_meta($hrac->ID, "sparing", 1);
        else  update_user_meta($hrac->ID, "sparing", 0);

        if($data["odebrat"]) update_user_meta($hrac->ID, "sparing", 0);
    }
}


function form_sparing_terms()
{
    return array(
        "message" => array(
            "requied" => true,
            "min_len" => 5,
            "type"    => "string"
        ),
        "place" => array(
            "requied" => true,
            "min_len" => 2,
            "type"    => "string"
        )
    );
}

function form_main_filters()
{
    return array(
        "message" => array("user_text_content"),
        "place" => array("user_text_content"),
    );
}

?>