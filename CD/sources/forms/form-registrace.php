<?php

function form_registrace_echo($hidden)
{
    ?>
    <h2>Registrace</h2>
    <div class="row">
        <div class="errorMes errorMesReg"></div>
    </div>
    <form id="registrationForm">
        <div class="row">
            <div class="group col-sm-6">
                <h4>Váš nick: </h4>
                <input class="form-control" id="registrationform_nick" type="text" name="registrace-nick" value="">
            </div>
            <div class="group col-sm-6">
                <h4>Váš email: </h4>
                <input class="form-control" id="registrationform_email" type="email" name="registrace-mail" value="">
            </div>
        </div>
        <div class="row">
            <div  class="group col-sm-6" >
                <h4> Vaše heslo:</h4>
                <input class="form-control" data-toggle="tooltip" title="Heslo musí mít aspoň 6 znaků" id="regpas" type='password' name='registrace-passwd'>
            </div>
            <div  class="group col-sm-6">
                <h4> Zopakujte heslo: </h4>
                <input class="form-control" data-toggle="tooltip" title="Hesla se neshodují" id="regpasc" type='password' name='registrace-passwd'>
            </div>
        </div>
        <div class="row">
            <div class="group col-sm-6">
                <h4> Vaše jméno: (nepovinné)</h4>
                <input class="form-control" id="registrationform_name" type='name' name='registrace-firstname' placeholder="Jméno" value='<?= $_POST["firstname"] ?>'>
            </div>
            <div class="group col-sm-6">
                <h4> Vaše příjmení: (nepovinné)</h4>
                <input class="form-control" id="registrationform_lastname" type='lastname' name='registrace-lastname' placeholder="Příjmení" value='<?= $_POST["lastname"] ?>'>
            </div>
        </div>
        <div class="row">
            <div class="group col-sm-6">
                <h4>Vaše pohlaví</h4>
                <div class="row gender">
                    <input type="radio" id="registrace_genderm" name="registrace-gender" value="m" checked>muž
                    <input type="radio" id="registrace_genderf" name="registrace-gender" value="f">žena
                </div>
            </div>
            <div class="group col-sm-6"> <!--TODO upravit stat-->
                <h4> Země: </h4>
                <select class="form-control" id="country2" name="registrace-country">
                    <?php
                    // Countries
                    $country_arr = get_countries();

                    // given the id of the <select> tag as function argument, it inserts <option> tags
                    echo "<option value=''>Vyber zemi</option>";
                    foreach ($country_arr as $c){
                        echo "<option value='{$c}' ".($c == "Czech Republic" ? "selected" : "").">{$c}</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row podminky">
            <input type="checkbox" name="podminky">
            <span class="podminky">
                Souhlasím s <a target="_blank" href="<?= get_permalink(OBCHODNI_PODMINKY) ?>" >obchodními podmínkami</a> a se zpracováním osobních údajů
            </span>
        </div>
        <div class="row">
            <div class="btn-group col-sm-6">
                <?= isset($_GET["alfa"]) && $_GET["alfa"] == "tester" ?
                    "<input class='btn btn-default' type='submit' id='registrationform_submit' name='reg' value='Registrovat se'>" :
                    "<span class='btn btn-default' data-toggle='tooltip' data-placement='bottom' data-original-title='Registrace bude možná brzy! Sleduj info na našich sociálních sítích.'>Registrace</span>"
                ?>
                <input class="btn btn-default" type='button' id="registrationform_login_btn" name='reg' value='Zpět na přihlášení'>
            </div>
        </div>
        <?= isset($_GET["alfa"]) && $_GET["alfa"] == "tester" ? "<input type=hidden name=registrace-alfa value=tester>" : "" ?>
        <?= $hidden ?>
    </form>
    <?php
}



function form_registrace_save($data, $errors)
{
    if(!$errors AND !is_user_logged_in())
    {
        //kontrola zda je možné se registrovat (přes není pravda, že..)
        if(!( get_option("users_can_register", false) || (isset($data["alfa"]) && $data["alfa"] == "tester") )){
            die(json_encode("Registrace je momentálně zakázána."));
        }

        if( username_exists( $data["nick"] ) ){
            die(json_encode("nick_exists"));
        }
        if( email_exists( $data["mail"] ) ){
            die(json_encode("mail_exists"));
        }


        $userID = wp_create_user( $data["nick"], $data["passwd"], $data["mail"]);

        $user_id_role = new WP_User($userID);
        $user_id_role->set_role('hrac');

        update_user_meta($userID, 'first_name', isset($data["firstname"]) ? $data["firstname"] : "");
        update_user_meta($userID, 'last_name', isset($data["lastname"]) ? $data["lastname"] : "");
        update_user_meta($userID, 'country', $data["country"]);

        add_user_meta( $userID, "sex", $data["gender"], true );
        if((isset($data["alfa"]) && $data == "tester")) add_user_meta( $userID, "tester", true );


        add_user_meta( $userID, "aktivace", false, true ); //hash se vytvoří při generování odkazu

        $user = wp_signon(array("user_login" => $data["nick"], 'user_password' => $data["passwd"]), false);

        if(is_wp_error($user))
        {
            die(json_encode($user->get_error_message()));
        }
        else
        {
            $hrac = new hrac($userID);
            return $hrac->sendActivationEmail();
        }

    }

    if( is_this_error($errors, "mail", "type") ) die(json_encode("Musíte zadat existující e-mailovou adresu"));

    return false;
}


function form_registrace_terms()
{
    return array(
        "nick" => array(       //název inputu (v html to je name='example-jmeno')
            "requied" => true,      //je to vyžadováno (není potřeba uvádět, ty názvy, co v podmínkách nemají zastoupení jsou volitelné - např. name='example-mail' je volitelný)
            "min_len" => 3,          //minimální délka
            "type"    => "string"   //datový typ - string | numeric | array
        ),
        "mail" => array(
            "requied" => true,
            "type"    => "email"
        ),
        "gender" => array(
            "requied" => true
        ),
        "passwd" => array(
            "requied" => true,
            "min_len" => 5,
        ),
        "country" => array(
            "requied" => true,
            "min_len" => 2,
            "type"    => "string"
        ),
        //nepovinné
        "firstname" => array(
            "requied" => false,
            "type"    => "string"
        ),
        "lastname" => array(
            "requied" => false,
            "type"    => "string"
        ),
    );
}

function form_main_filters()
{
    return array(
        "nick" => array("user_text_content"),
        "country" => array("user_text_content"),
        "firstname" => array("user_text_content"),
        "lastname" => array("user_text_content"),
    );
}


?>